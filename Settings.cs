namespace Parking
{
    public static class Settings
    {
        public static int ParkingBalanceDefault = 0;
        public static int MaxCarAmount = 10;
        public static int WithdrawalTime = 5;
        public static double LightCarPrice = 2;
        public static double TruckPrice = 5;
        public static double BusPrice = 3.5;
        public static double MotorcyclePrice = 1;
        public static double fee = 2.5;
    }
}