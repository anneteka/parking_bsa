namespace Parking
{
    public enum CarType
    {
        LightCar,
        Truck,
        Bus,
        Motorcycle
    }
}