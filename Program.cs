﻿using System;

namespace Parking
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Parkinglot p = Parkinglot.getInstance();
            
            Car c = new Car(CarType.Bus, 10);
            Car c2 = new Car(CarType.Motorcycle, 25);
            p.addCar(c);
            p.addCar(c2);
            p.debit(c, 20);
            p.printCars();
            
            
            
            p.addTransaction(new Transaction(1,10,DateTime.Now));
            p.log();
            //no UI :(
        }
    }
}