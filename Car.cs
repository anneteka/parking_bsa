using System;
using System.CodeDom.Compiler;

namespace Parking
{
    public class Car
    {
        private static int carAmount = 0;
        private CarType type;
        private int id;
        private double moneyAmount;

        public Car(CarType type, int money)
        {
            id = carAmount++;
            moneyAmount = money;
            this.type = type;
        }

        public Transaction pay()
        {
            double withdraw = 0;
            switch (type)
            {
                case CarType.LightCar:
                    withdraw = Settings.LightCarPrice;
                    break;
                case CarType.Bus:
                    withdraw = Settings.BusPrice;
                    break;
                case CarType.Motorcycle:
                    withdraw = Settings.MotorcyclePrice;
                    break;
                case CarType.Truck:
                    withdraw = Settings.TruckPrice;
                    break;
            }

            double ret = moneyAmount < withdraw ? withdraw * 2.5 : withdraw;
            moneyAmount -= ret;
            return new Transaction(id, ret, DateTime.Now);
        }

        public override bool Equals(object obj)
        {
            if (id==((Car) obj).id)
            return true;
            return false;
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public double MoneyAmount
        {
            get => moneyAmount;
            set => moneyAmount = value;
        }

        public override string ToString()
        {
            return "ID: "+id+", type: "+type.ToString()+", money: "+moneyAmount;
        }
    }
}