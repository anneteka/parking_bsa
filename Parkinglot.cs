using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Parking
{
    class Parkinglot
    {
        private static Parkinglot instance;

        private List<Transaction> transactions;
        private List<Car> cars;
        private int balance;

        private Parkinglot()
        {
            transactions = new List<Transaction>();
            cars = new List<Car>();
            balance = 0;
        }

        public static Parkinglot getInstance()
        {
            if (instance == null)
                instance = new Parkinglot();
            return instance;
        }

        public bool addCar(Car car)
        {
            if (cars.Count > Settings.MaxCarAmount) return false;
            else cars.Add(car);
            return true;
        }

        public void addTransaction(Transaction t)
        {
            transactions.Add(t);
        }

        public void removeCar(Car c)
        {
            cars.Remove(c);
        }

        public int Balance
        {
            get => balance;
        }

        public void resetTransactions()
        {
            transactions = new List<Transaction>();
        }

        public double getIncome()
        {
            double sum = 0;
            foreach (Transaction t in transactions)
            {
                sum +=  t.MoneyWithdrawn;
            }

            return sum;
        }

        public int getFree()
        {
            return Settings.MaxCarAmount - cars.Count;
        }

        public int getOccupied()
        {
            return cars.Count;
        }

        public void printTransactions()
        {
            Console.WriteLine("Transactions: ");
            foreach (Transaction t in transactions)
            {
                Console.WriteLine(t);  
            }
        }

        public void printCars()
        {
           Console.WriteLine("CARS: ");
            foreach (Car c in cars)
            {
                Console.WriteLine(c);  
            }
        }

        public void debit(Car car, int amount)
        {
            Car temp = cars.Find(x => x.Equals(car));
            temp.MoneyAmount += amount;
        }

        public void log()
        {
            System.IO.StreamWriter objWriter = new System.IO.StreamWriter("Transactions.log"); 
            foreach (Transaction t in transactions)
            {
                objWriter.Write(t); 
            }
            objWriter.Close();
            resetTransactions();
        }
    }
}

